//import { useEffect, useState } from 'react';
import {useParams} from "react-router-dom";

import "./Detail.css"


function Detail(props) {
    const { productId } = useParams(); //To get the value of a path variable in a url.
	
	const DBGLOG = (msg, object)  => {
        //console.log(msg);
    } 
	
	const attributeMaps = [
		{"id" : "country", "label" : "Country"},
		{"id" : "createdDate", "label" : "Created Date"},
		{"id" : "expiryDate", "label" : "Expiry Date"},
		{"id" : "orgType", "label" : "Organization Type"},
		{"id" : "natureOfActivity", "label" : "Nature Of Activity"},
		{"id" : "innovator", "label" : "Innovator"},
		{"id" : "keywords", "label" : "Keywords"}
		];

    DBGLOG("productId from path variable = " + productId);

    const getProductByIdOrSemanticURL = (products => {
        var fRet = null;
        var semanticURL = "/detail/" + productId;
        products.map(product => {
            if(product.id === productId || product.link === semanticURL) {
                fRet = product;
                return fRet;
            }

        });
        return fRet;
    });
	
	const product = getProductByIdOrSemanticURL(props.products);
	window.scrollTo(0, 0);

/*
    useEffect(() => {
        window.scrollTo(0, 0);

        Promise.all([
            fetch('/data/categories.json'),
            fetch('/data/products.json')
        ]).then(responses => {
            // Get a JSON object from each of the responses
            return Promise.all(responses.map(function (response) {
                return response.json();
            }));
        }).then(data => {
            // Log the data to the console
            // You would do something with both sets of data here
            console.log("Result")
            console.log(data);
            // data[0] => response from categories.json
            // data[1] => response from products.json

            //mergeData(data[0], data[1]);
            //setData(data[1]);
            // productService.setProducts(data[1]);
            //aggregateData (data[0], data[1]);
            var product = getProductByIdOrSemanticURL(data[1]);
            console.log("Product Object");
            console.log(product);
            setProduct(product);
        }).catch(error => {
            // if there's an error, log it
            console.error("Home Page, Error fetching data : ", error) ;
            //setError(error);
        }).finally (() => {
            // setLoading(false)
        });
    }, []);  
*/
    const ProductOverview = (props) => {
		
		let attributes =[];
		
		attributeMaps.map( attributeMap => {
			Object.keys(props.product).map(key => {
				if (key === attributeMap.id) {
					var attribute = {};
					attribute.name = attributeMap.label;
					attribute.value = props.product[key];
					attributes.push(attribute);
				}
			});
		})
		
        return attributes.map( attribute => {
			return (
				<ul>
					<li>
						<div className="rndo-product-attributes">
							<p>{attribute.name}</p>
						</div>
						<div className="rndo-product-attribute-values">
							<p>{attribute.value}</p>
						</div>										
					</li>								
				</ul>			
			)
		});
    }    	
	
	const Descriptions = (props) => {
		return props.descriptions.map( description => {
			return (
				<div>
					<p>{description}</p>
				</div>
			)
		})
	}

    return (
        <>
            <div className="container rndo-section rndo-product-detail">
				<div className="row">
					<div className="col-md-12 col-sm-12 rndo-product-title">
						<h1> { product.name } </h1>
					</div>
				</div>
                <div className="row">
                    <div className="col-md-8 col-sm-8 rndo-details-panel">
                        
						{/* begin */}
						<div className="job-detail-description">
												
						<h3 className="mt0 mb10"> Summary</h3>
						<p>{product.summary}</p>

						<h3 className="mt0 mb10"> Description</h3>
						{
							product.descriptions?.map( description => 
								<p>{description}</p>
							)
						}

						{
							product.sections?.map( section => 
								<>
									<h3 className="mt0 mb10"> {section.title} </h3>
									<Descriptions descriptions={section.descriptions}/>
								</>
							)
						}		
					</div>
					{/* end */}
					
                    </div>
					
					
                    <div className="col-md-4 col-sm-4 rndo-details-attr-panel" >
						<div className="row">
							<h3 className="rndo-details-attr-overview">Overview</h3>
						</div>
						
						<div className="row">
							<div className="col-md-12 col-sm-12" align="center">
								<ProductOverview product={product}/>
							</div>
													
						</div>
                    </div>
					
					
                </div>
            </div>
        </>
    );
}
export default Detail;