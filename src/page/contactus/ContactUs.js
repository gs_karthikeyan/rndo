import { useEffect, useState } from "react";
import emailjs from 'emailjs-com';

import "./ContactUs.css"

import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure()

function ContactUs(props) {
    const contactInfo = props.contactInfo;
	
	const STR_SUCCESS_EMAIL_SENT = "The email has been sent successfully.";
	const STR_FAILURE_EMAIL_SENT = "Oops! something went wrong. Please try again later.";
	
	const SERVICE_ID = "service_p086r6t";
	const TEMPLATE_ID = "template_xnqpg0o";
	const USER_ID = "user_ac8gPsFlZ4pphxGvbTjT3";	
	
	const [ btnState, setBtnState] = useState("Send");
	const [ error, setError] = useState({});
	
	const errorTxtList = {
		"first_name" : "Please enter a valid first name",
		"last_name" : "Please enter a valid last name",
		"from_email" : "Please enter a valid email",
		"message" : "Please enter a valid message"
	}
	
	const validateInputFields = (e) => {
		e.preventDefault();
		console.log("validateInputFields");
		var errors = {};
		errors["error"] = false;
		
		Array.from(document.querySelectorAll("input")).forEach(
			input => {
 				
				if (input.name === "first_name" || input.name === "last_name" || input.name === "from_email") {				
					if (!input.value.trim()) {
						errors["error"] = true;
						errors[input.name] = errorTxtList[input.name];
					}
					
					if (input.name === "from_email" && input.value.trim()) {
						if( /(.+)@(.+){2,}\.(.+){2,}/.test(input.value.trim()) ){
						  // valid email
						} else {
							errors["error"] = true;
							errors[input.name] = errorTxtList[input.name];
						}					
					}			
				}				
			}
		  );		
		  
		Array.from(document.querySelectorAll("textarea")).forEach(
			textarea => {
				if (!textarea.value.trim()) {
					errors["error"] = true;
					errors["message"] = errorTxtList["message"];
				}
			}
		  );

		  
		  console.log(errors["error"]);
		  if (errors["error"]) {
			setError (errors);
		  } else {
			setError ({});
			sendEmail(e);
		  }
	}
	
	const resetInputFields = () => {
		Array.from(document.querySelectorAll("input")).forEach(
			input => (input.value = "")
		  );				  
		  
		Array.from(document.querySelectorAll("textarea")).forEach(
			textarea => (textarea.value = "")
		  );
	}
	
	const sendEmail = (e) => {
		e.preventDefault();
		setBtnState("Sending...");
		emailjs.sendForm(SERVICE_ID, TEMPLATE_ID, e.target, USER_ID)
		  .then(
			  (result) => {
				  console.log(result.text);
				  toast.success(STR_SUCCESS_EMAIL_SENT, {position: toast.POSITION.BOTTOM_CENTER, autoClose:3000});
				  resetInputFields();
				  setBtnState("Send");
			  }, 
			  (error) => {
				  console.log(error.text);
				  toast.error(STR_FAILURE_EMAIL_SENT, {position: toast.POSITION.BOTTOM_CENTER, autoClose:3000});
				  setBtnState("Send");
			  }
			);		
	}

    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])    

    return (
        <>
            <div className="container rndo-contact-us">
                <div className="row" align="left">
                    <div className="col-md-12 col-sm-12">
                        <h3>Contact Us</h3>
                    </div>
                </div>

                <div className="row" align="left">
                    <div className="col-md-8 col-sm-8">
						<form className="contact-form" onSubmit={validateInputFields}>
							<div className="row">
								<div className="col-md-6 col-sm-6">
									<p>First Name (*)</p>
									<input type="text" name="first_name"></input>
									<label className="rndo-error">{error["first_name"]}</label>
								</div>
								<div className="col-md-6 col-sm-6">
									<p>Last Name (*)</p>
									<input type="text" name="last_name"></input>
									<label className="rndo-error">{error["last_name"]}</label>									
								</div>                            
							</div>

							<div className="row">
								<div className="col-md-12 col-sm-12">
									<p>Your Email (*)</p>
									<input type="text" name="from_email" ></input>
									<label className="rndo-error">{error["from_email"]}</label>
								</div>                           
							</div>

							<div className="row">
								<div className="col-md-12 col-sm-12">
									<p>Tell us more about yourself (*)</p>
									<textarea name="message"></textarea>
									<label className="rndo-error">{error["message"]}</label>
								</div>                           
							</div>           

							<div className="row">
								<div className="col-md-12 col-sm-12" align="center">
									<button type="submit" className="btn btn-call-to-act rndo-them-button-fire rndo-btn-send">{btnState}</button>
									{/* <button className="btn btn-call-to-act rndo-them-button-cancel rndo-btn-cancel">Cancel</button> */}
								</div>                                                        
							</div>      
						</form>
                    </div>

                    <div className="col-md-4 col-sm-4">
                        <div className="row">
                            <div className="col-md-12 col-sm-12">
                                <p>{contactInfo.addr}</p>
                                <label>{contactInfo.email}</label>                                
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12 col-sm-12"> 
                                <iframe title="RNDO Location" className="rndo-google-map" src="https://www.google.com/maps?q=RnDO%20LTD%2C%20Kemp%20House%2C%20160%2C%20City%20Road%2C%20London%2CEC1V%202NX%2C%20UK&output=embed&hl=en&z=14"></iframe>
                            </div>
                        </div>

                    </div>                    
                </div>
            </div>

        </>
    );
}
export default ContactUs;