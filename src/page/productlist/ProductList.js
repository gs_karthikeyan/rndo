import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "./ProductList.css"
// import CategoryService from "../../service/CategoryService"
import Utils from "../../service/Utils"

// import ProductService from "../../service/ProductService";

function ProductList (props) {

    const STR_NO_RESULTS_FOUND = "No results found";

    let productListResponse = {
        "products"  :   props.products,

        "filters"   :   {
            "keyword"       : "",
            "regions"       : [],
            "categories"    : props.categories
        }};

    const [data, setData]   = useState(productListResponse);
    // const [error, setError]     = useState();
    const [loading, setLoading] = useState(true);
    const [searchParamObj, setSearchParamObj] = useState({
        "keyword"   : "",
        "region"    : "",
        "category"  : ""
    });

    var utils = new Utils();
    // var productService = new ProductService();

    const DBGLOG = (msg, object)  => {
        //console.log(msg);
    } 

    const doFilter = () => {
        DBGLOG("doFilter.." + searchParamObj.keyword + "~~~" + searchParamObj.region + "~~~" + searchParamObj.category );

        var productList = JSON.parse(localStorage.getItem('productList'));
        var filterResult = productList;

        if (searchParamObj.category) {
            filterResult.products = getProductsByCategoryId(filterResult.products, productList.filters.categories, searchParamObj.category);
			DBGLOG("searchParamObj.category");
			DBGLOG(filterResult.products);
			DBGLOG("category len = " + filterResult.products.length);
        }

        if (searchParamObj.region) {
            filterResult.products = getProductsByRegion(filterResult.products, searchParamObj.region);
			DBGLOG(" region len = " + filterResult.products.length);
        }

        if (searchParamObj.keyword) {
            filterResult.products = getProductsByKeyword(filterResult.products, searchParamObj.keyword);
			DBGLOG("keyword len = " + filterResult.products.length);
        }        
		
		setData(filterResult);
    };

    const onInputChange = (event) => {
        let searchParam = {
            "keyword" : searchParamObj.keyword,
            "region" : searchParamObj.region,
            "category" : searchParamObj.category            
        }

        if (event.target.id === "keyword") {
            searchParam.keyword = event.target.value;
        } else if (event.target.id === "region") {
            searchParam.region = event.target.value;
        } else if (event.target.id === "category") {
            searchParam.category = event.target.value;
        }
        setSearchParamObj(searchParam);
    }

    const aggregateData = (categories, products) => {

        // DBGLOG("Get Products from ProductService");
        // DBGLOG(productService.getProducts());
        DBGLOG("aggregateData");
        productListResponse.products = products;
        productListResponse.filters.categories = categories;

        let regions = [];
        products.map ( product => {
            if ( product.country ) {
                regions.push(product.country);
            }            
        } )

        productListResponse.filters.regions = regions;

        localStorage.setItem('productList',JSON.stringify(productListResponse));
        DBGLOG(productListResponse);
        //setData(productListResponse);
    }

    const getProductsByKeyword = (products, keyword) => {
        var fRetProducts = [];

        products.map ( product => {
            if (product.keywords.toLowerCase().includes(keyword.toLowerCase()) ||
				product.name.toLowerCase().includes(keyword.toLowerCase())
				) {
                fRetProducts.push(product);
            }
        });
        return fRetProducts; 
    }    

    const getProductsByRegion = (products, region) => {
		//DBGLOG("getProductsByRegion");
		//DBGLOG(products);
        var fRetProducts = [];
        products.map ( product => {
			//DBGLOG(product);
            if (product.country.toLowerCase().includes(region.toLowerCase())) {
                fRetProducts.push(product);
            }
        });
        return fRetProducts; 
    }

    const getProductsByCategoryId = (products, categories, categoryId) => {
        var pdts = [];
        var category = getCategoryById(categories, categoryId);
		DBGLOG("getProductsByCategoryId..");

		//DBGLOG(category);
		
		if (category.productIds && category.productIds.length > 0) {
			category.productIds.map( productId => {
				DBGLOG("productId = " + productId);
				var product = getProductById(products, productId);
				pdts.push(product);
			})
		} else {
			pdts = products;
		}
        return pdts;
    }

    const getCategoryById = (categories, id) => {
        let fRet = null;

        categories.map ( category => {
            if (category.id == id) {
                fRet = category;
                return fRet;
            }
        });
        return fRet;       
    }

    const getProductById = (products, id) => {
		
        let fRet = null;

        products.map ( product  => {
            if (product.id == id) {
                fRet = product;
                return fRet;
            }
        });
        return fRet;
    }

    DBGLOG("****************");
    DBGLOG(data);
    DBGLOG("***********");

	aggregateData (props.categories, props.products);
	window.scrollTo(0, 0);
	/*
    useEffect(() => {
        DBGLOG("useEffect");
        Promise.all([
            fetch('/data/categories.json'),
            fetch('/data/products.json')
        ]).then(responses => {
            // Get a JSON object from each of the responses
            return Promise.all(responses.map(function (response) {
                return response.json();
            }));
        }).then(data => {
            // Log the data to the console
            // You would do something with both sets of data here
            DBGLOG("Result")
            DBGLOG(data);
            // data[0] => response from categories.json
            // data[1] => response from products.json

            //mergeData(data[0], data[1]);
            //setData(data[1]);
            // productService.setProducts(data[1]);
            aggregateData (data[0], data[1]);
            
        }).catch(error => {
            // if there's an error, log it
            console.error("Home Page, Error fetching data : ", error) ;
            //setError(error);
        })
        .finally (() => {
            DBGLOG("finally block called");
            setLoading(false)
        });
    }, []);   
   
   */
 
    const SearchResults = (props) => {

        DBGLOG ("searchResults");
        DBGLOG (props.products);

        if (!props.products || props.products.length <=0) {
            return [
                <div className="col-md-4 col-sm-6 rndo-search-no-result" >
                    <p> {STR_NO_RESULTS_FOUND} </p>
                </div>
            ]
        } else {
            return props.products.map((product, key) => {  
                return (
                    <div key={key} className="col-md-4 col-sm-6 rndo-list-cell" >
                        <Link to={product.link}>
                            <div className="grid-view brows-job-list">
                                <div className="rndo-card-image" align="center">
                                    <img src={product.img} className="img-responsive" alt="" />
                                </div>
                                <div className="brows-job-position" align="left">
                                    <h3>{product.name}</h3>
                                    {/* <p><span>Google</span></p> */}
                                </div>
                                {/* <div className="job-position" align="left">
                                    <span className="job-num">{product.summary}</span>
                                </div> */}
                                {/* <div className="brows-job-type">
                                    <span className="full-time">Full Time</span>
                                </div> */}
                                <ul className="grid-view-caption">
                                    <li className="rndo-pipe"> 
                                        <div className="brows-job-location">
                                            <p><i className="fa"></i>{product.country}</p>
                                        </div>
                                    </li>
                                    <li className="rndo-pipe">
                                        <p><span className="brows-job-sallery"><i className="fa"></i>{ utils.getCreateDateInTextFormat(product.createdDate) }</span></p>
                                    </li>
                                    <li>
                                        <p><span className="brows-job-sallery"><i className="fa"></i>expires in 2 days</span></p>
                                    </li>                                        
                                </ul>
                                {/* <span className="tg-themetag tg-featuretag">Premium</span> */}
                            </div>
                        </Link>
                    </div>
                )
            }	
            ); 
        }
    }

    return (
        <>
            <div className="container rndo-section rnd-list">
                <div className="row">
                    <div className="wrap-search-filter">
                       
                            <div className="col-md-4 col-sm-4">
                                <input id="keyword" type="text" className="form-control" placeholder="Keyword" onChange={onInputChange} value={searchParamObj.keyword}/>
                            </div>
                            <div className="col-md-3 col-sm-3">
                                {/* <input id="region" type="text" className="form-control" placeholder="Region" onChange={onInputChange} value={searchParamObj.region}/> */}
                                <select className="form-control" id="region" onChange={onInputChange} value={searchParamObj.region} defaultValue={searchParamObj.region}>
                                   <option value="">Choose Region</option>
                                    {
                                        data.filters.regions.map ( region => 
                                            <option value={region}>{region}</option>
                                            )    
                                    }                                   
                                </select>

                            </div>
                            <div className="col-md-3 col-sm-3">
                                <select className="form-control" id="category" onChange={onInputChange} value={searchParamObj.category} defaultValue={searchParamObj.category}>
                                   <option value="">Choose Category</option>
                                    {
                                        data.filters.categories.map ( category => 
                                            <option value={category.id}>{category.name}</option>
                                            )    
                                    }                                   
                                </select>

                            </div>
                            <div className="col-md-2 col-sm-2">
                                <button className="btn btn-primary full-width" onClick={doFilter}>Filter</button>
                            </div>
                    </div>


                </div>


                <div className="row">
					<SearchResults products={data.products}/>
                </div>
            </div>
        </>
    );
}
export default ProductList;