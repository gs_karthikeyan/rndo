import { Link } from 'react-router-dom';
import './Home.css';
import Utils from "../../service/Utils"

function Home(props) {
	const STR_DEFAULT_BANNER_BG_COLOR = "#3c9cb2";
	
	const data = props.homeContent || [];
	const categories = props.categories || [];
	const products = props.products || [];	
	var utils = new Utils();
	
	const DBGLOG = (msg) => {
		//console.log(msg);
	}
	
	const getFeaturedCategories = (categoryIds) => {
		var featuredCategories = [];
		for (var i = 0; i < categoryIds.length; i++) {
			for (var j = 0; j < categories.length; j++) {
				if (categories[j].id === categoryIds[i]) {
					featuredCategories.push(categories[j]);
				}
			}
		}
		return featuredCategories;
	}
	
	const getFeaturedProducts = (productIds) => {
		var featuredProducts = [];
		for (var i = 0; i < productIds.length; i++) {
			for (var j = 0; j < products.length; j++) {
				if (products[j].id === productIds[i]) {
					featuredProducts.push(products[j]);
				}
			}
		}
		return featuredProducts;
	}
	
	data.featuredCategories = getFeaturedCategories(data.featuredCategoryIds);
	data.featuredProducts = getFeaturedProducts(data.featuredProductIds);
	
	DBGLOG("Featured Items....")
	DBGLOG(data.featuredCategories);
	DBGLOG(data.featuredProducts);


    return (
        <>
            {/* Section 1 : Video */}
            <div>
                <div className="container">
					<video className="rnd-home-video" autoPlay={false} controls={true} muted={false}><source src={ data.videoURL } type="video/mp4" /></video>
                </div>
            </div>

            {/* Section 2 : How we work Together */}
            <div className="how-it-works rndo-home-section">
                <div className="container">
                    <div className="row" data-aos="fade-up">
                        <div className="col-md-12">
                            <div className="main-heading">
                                <h2>How we work <span className="rndo-theme-section-title">Together</span></h2>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        {
                            data.howItWorks?.map((item, key) => 
                                <div key={key} className="col-md-4 col-sm-4">
                                    <div className="working-process">
                                        <span className="process-img">
                                            <img alt="" className="img-responsive" src={item.icon} />
                                            <span className="process-num">{key}</span>
                                        </span>

                                        <h4>{item.name}</h4>
                                        <p>{item.description}</p>
                                    </div>
                                </div>                                
                                
                            )
                        }
                    </div>
                </div>
            </div>
                    
            {/* Section 3 : Banner Text Grid Display */}
            <div className="call-to-act">
                <div className="container-fluid">

                {
                    data.bannerText?.map((banner, key) => 
                        <div key={key} className="col-md-6 col-sm-6 no-padd bl-dark desc-card" style={{backgroundColor : banner.bgColor ? banner.bgColor : STR_DEFAULT_BANNER_BG_COLOR}}>
                            <div className="call-to-act-caption">
                                <h2 className="rndo-home-banner-name">{banner.name}</h2>
                                <h3 className="rndo-home-banner-desc">{banner.description}</h3>
                                <Link className="btn bat-call-to-act" to={banner.buttonLink}>{banner.buttonName}</Link>
                            </div>
                        </div>                        
                        )
                }

                </div>
            </div>

             {/* Section 4 : Category Section */}
            <div className="gray rndo-home-section">
                <div className="container">
                    <div className="row">
                        <div className="main-heading">
                            <h2>Innovative Technologies by <span className="rndo-theme-section-title">Industry sector</span></h2>
                        </div>
                    </div>

                    <div className="row">
                        {
                            data.featuredCategories?.map((category, key) =>
                                <Link to={category.link}>
                                    <div key={key} className="col-md-3 col-sm-6">
                                        <div className="category-box" data-aos="fade-up">
                                            <div className="category-desc">
                                                <div className="category-icon"><img alt="" className="img-responsive" src={category.img} /></div>

                                                <div className="category-detail category-desc-text">
                                                <h4>{category.name}</h4>

                                                <p className="rndo-display-none">{category.description}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>   
                                </Link>                                
                            )
                        }             
                    </div>
                </div>
            </div>
			
			
             {/* Section 4.1 : Featured Products Section */}
            <div className="gray rndo-home-section">
                <div className="container">
                    <div className="row">
                        <div className="main-heading">
                            <h2>Hot <span className="rndo-theme-section-title">Star</span></h2>
                        </div>
                    </div>

                    <div className="row">
						{
							data.featuredProducts?.map((product, key) =>   
								<div key={key} className="col-md-4 col-sm-6 rndo-list-cell" >
									<Link to={product.link}>
										<div className="grid-view brows-job-list">
											<div className="rndo-card-image" align="center">
												<img src={product.img} className="img-responsive" alt="" />
											</div>
											<div className="brows-job-position" align="left">
												<h3>{product.name}</h3>
											</div>
											<ul className="grid-view-caption">
												<li className="rndo-pipe"> 
													<div className="brows-job-location">
														<p><i className="fa"></i>{product.country}</p>
													</div>
												</li>
												<li className="rndo-pipe">
													<p><span className="brows-job-sallery"><i className="fa"></i>{ utils.getCreateDateInTextFormat(product.createdDate) }</span></p>
												</li>
												<li>
													<p><span className="brows-job-sallery"><i className="fa"></i>expires in 2 days</span></p>
												</li>                                        
											</ul>
											{/* <span className="tg-themetag tg-featuretag">Premium</span> */}
										</div>
									</Link>
								</div>
							)
						}                        
                     
                    </div>
                </div>
            </div>			
            
            {/* Section 5 Call to action */}
            <div className="call-to-act-wrap rndo-home-section rnd-bg-color">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="call-to-act">
                                <div className="call-to-act-head">
                                    <h3>Want to become a subject matter expert consultant?</h3>
                                    <span>Accelerate growth through visibility and collaboration</span>
                                </div>
                                <Link to="/contactus" className="btn btn-call-to-act">Contact US</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </>
    );
}
export default Home;