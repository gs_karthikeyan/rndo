import "./Funding.css"


function Funding () {


    return (
        <>
            <div className="rndo-home-section rndo-funding">
                <div className="row">
                    <div style={{backgroundImage : "url(/assets/img/about-banner.png)"}} className="page-cover page-cover--default has-image">
                        <h1 className="page-title cover-wrapper">Funding</h1>
                    </div>      
                </div>
                <div className="row">
                    <div id="primary" className="container">
                        <div className="content-area rndo-section">

                            <main id="main" className="site-main" role="main">
                                <article id="post-487" className="post-487 page type-page status-publish has-post-thumbnail hentry content-box content-box-wrapper">
                                    
                                        {/* <h3><strong>Why RNDO</strong></h3> */}
                                        <p>To leverage your technology development goals and improve  business outcomes, we can assist in finding and accessing a range of funding opportunities, including;</p>
                                        {/* <p>Using By Using RNDO overcome limitations such as&nbsp;</p> */}
                                        <ul>
                                            <li>Public funding from regional, national and international bodies, such as Innovate UK, Horizon 2020, etc</li>
                                            <li>Proof of concept and scale up funds from private investors</li>
                                            <li>VC funding.</li>
                                            <li>Crowd funding</li>                                            
                                        </ul>

                                        <p className="rndo-para-overrides-line-ht">RNDO can adapt its services based on your unique Technology Readiness Level or   organisation type. Whether you are a SME, Start-up, Large Enterprise, Research Institute, Innovator or Entrepreneur, we can help with all stages of your strategy. You might also wish to pick specific areas for support such as those shown below;</p>
                                        {/* <p>RNDO also offers bespoke options through which the user may tailor the output to individual needs. For technology providers, the RNDO successfully mirrors the above, providing a large and level playing field on which to showcase and engage with multi sector end users of all sizes.&nbsp;</p> */}

                                        {/* <p>We are committed to deliver;</p> */}


                                        <ol>
                                            <li>Identifying suitable opportunities</li>
                                            <li>Mapping with your end goals</li>
                                            <li>Bid writing</li>
                                            <li>Consortia building</li>
                                            <li>Consortium management</li>
                                            <li>Project management</li>
                                            <li>IP protection</li>
                                            <li>Deliverables and implementation</li>
                                            <li>Exploitation and route to market</li>
                                            <li>Business plan</li>
                                            <li>Marketing and promotional content such as videos, flyers, branding, trademark registration, articles in journals,etc</li>
                                        </ol>

                                </article>

                                
                            </main>

                        </div>
		            </div>                              
                </div>
            </div>
        </>
    );
}

export default Funding;