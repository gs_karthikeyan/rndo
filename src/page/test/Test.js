import { useEffect, useState } from "react";

function Test() {

    const [data, setData] = useState([]);

    const DBGLOG = (msg) => {
        console.log(msg);
    }

    const aggregateData = (data) => {

        // DBGLOG("Get Products from ProductService");
        // DBGLOG(productService.getProducts());
        DBGLOG("aggregateData");

        localStorage.setItem('testData',JSON.stringify(data));
        
        setData(data);
    }    

    useEffect(() => {
        DBGLOG("useEffect");
        Promise.all([
            fetch('/data/categories.json'),
            fetch('/data/products.json')
        ]).then(responses => {
            // Get a JSON object from each of the responses
            return Promise.all(responses.map(function (response) {
                return response.json();
            }));
        }).then(data => {
            // Log the data to the console
            // You would do something with both sets of data here
            DBGLOG("Result")
            DBGLOG(data);
            // data[0] => response from categories.json
            // data[1] => response from products.json

            //mergeData(data[0], data[1]);
            //setData(data[1]);
            // productService.setProducts(data[1]);
            aggregateData (data[0], data[1]);
            
        }).catch(error => {
            // if there's an error, log it
            console.error("Home Page, Error fetching data : ", error) ;
            //setError(error);
        })
        .finally (() => {
            DBGLOG("finally block called");
            //setLoading(false)
        });
    }, []); 

    const UI = (props) => {
        DBGLOG("ui");
        return [
            <p> {JSON.stringify(props)} </p>
        ]
    }

    return(
        <>
            <UI data={data}/>
        </>
    );

}

export default Test;