import { useEffect } from 'react';
import './About.css';

function About() {

    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])

    return (
        <>
            <div className="rndo-home-section">
                <div className="row">
                    <div style={{backgroundImage : "url(/assets/img/about-banner.png)"}} className="page-cover page-cover--default has-image">
                        <h1 className="page-title cover-wrapper">About Us</h1>
                    </div>      
                </div>
                <div className="row">
                    <div id="primary" className="container">
                        <div className="content-area rndo-section">

                            <main id="main" className="site-main" role="main">
                                <article id="post-487" className="post-487 page type-page status-publish has-post-thumbnail hentry content-box content-box-wrapper">
                                    
                                        <h3>Our Mission</h3>
                                        <p><strong>O</strong>ur mission is to provide a world leading, user friendly, technology brokerage platform that will accelerate the finding, accuracy, and adoption of technology, and acts as a pulley on both sides of the “Innovation Chasm”.&nbsp;</p>

                                        <h3>Who we Are</h3>
                                        <p>We are a global network of experienced and aspirational scientists, engineers, universities, research institutes and businesses who have a&nbsp; passion for technology, innovation and engineering. We aim to connect people and ideas, who are often hidden by the “innovation chasm”, and break through technology and sector bunkers. This gap, between innovators and innovation adopters, is widely recognised as a prime factor in losing opportunities for innovative growth, across all sectors.&nbsp;&nbsp;</p>
                                        <p>Our tailored service portfolio offers a&nbsp; combination of value adding usage points, ranging from access to world experts, AI smart technology finding&nbsp; and interfacing, access to funding assistance and promotional services to increase your visibility. We provide every customer with bespoke project management support creating roadmaps for breaking down technology bunkers,and enabling clear and impartial choices. &nbsp;We strongly believe that a collaborative <em>and </em>single client approach will accelerate the widespread awareness, development and adoption of innovative ideas and technology. We focus on reducing risk and increase positive impacts for people and businesses around the World.</p>

                                        <h3><strong>Why RNDO</strong></h3>
                                        <p>RNDO is a one stop shop with extensive connections, saving you valuable time and money in identifying and finding solutions to your needs. We deliver&nbsp; efficiency, accuracy, best practice and time to market benefits to our Members. Our niche is to be business/customer driven and focussed on return on investment and value proposition in everything we do for our Members.</p>
                                        <p>Using By Using RNDO overcome limitations such as&nbsp;</p>
                                        <ul>
                                            <li>Biased technical advice and solutions.</li>
                                            <li>Lack of knowledgeable resources/expert opinion.</li>
                                            <li>Time.</li>
                                            <li>Hoarding of technologies by a few companies.</li>
                                            <li>Blinkered single sector use of innovation.</li>
                                            <li>Lack of visibility of innovators,applications and users globally.</li>
                                            <li>Cost of in-house resources with limited reach.</li>
                                        </ul>

                                        <p>RNDO presents a community platform that is broad, independent and proactive, delivering quick and accurate search and matchmaking choices between end users and technology providers. For innovation users it will facilitate access to wide trawl global information, ideas, technology needs, solutions and providers.&nbsp;</p>
                                        <p>RNDO also offers bespoke options through which the user may tailor the output to individual needs. For technology providers, the RNDO successfully mirrors the above, providing a large and level playing field on which to showcase and engage with multi sector end users of all sizes.&nbsp;</p>

                                        <p>We are committed to deliver;</p>



                                        <ol>
                                            <li>Accelerated rates of technology uptake.</li>
                                            <li>Improved rate of innovative ideas to market.&nbsp;</li>
                                            <li>Associated positive effect on productivity, new product development and employment</li>
                                        </ol>

                                </article>

                                
                            </main>

                        </div>
		            </div>                              
                </div>
            </div>
        </>
    );
}
export default About;