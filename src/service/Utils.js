function Utils () {
    const NUMBER_DAYS_IN_YEAR = 365;
    const NUMBER_DAYS_IN_MONTH = 30;
}

Utils.prototype = {
    getCreateDateInTextFormat : (date) => {
        const NUMBER_DAYS_IN_YEAR = 365;
        const NUMBER_DAYS_IN_MONTH = 30;

        let retText = "";
        const diffInMs = new Date() - new Date(date);
        let diffInDays = diffInMs / (1000 * 60 * 60 * 24);

        if (diffInDays > NUMBER_DAYS_IN_YEAR) {
            let year = Math.ceil(diffInDays / NUMBER_DAYS_IN_YEAR);
            retText = year + " year" + (year > 1 ? "s" : "") + " ago";

        } else if(diffInDays > NUMBER_DAYS_IN_MONTH){
            let month = Math.ceil(diffInDays / NUMBER_DAYS_IN_MONTH);
            retText = month + " month" + (month > 1 ? "s" : "") + " ago";

        } else {
            diffInDays = Math.ceil(diffInDays);

            if ( diffInDays == 0 ) {
                retText = "few times ago";
            } else if( diffInDays > 0 ) {
                retText = diffInDays + " day" + (diffInDays > 1 ? "s" : "") + " ago";
            }
        }
        return retText;
    }

}

export default Utils;