import { Switch, Route, BrowserRouter} from "react-router-dom";
import { useEffect, useState } from "react";

import './assets/css/plugins.css';
import './assets/css/style.css';
import './assets/css/dark-red-style.css';
import './App.css';

import Home from "./page/home/Home";
import ProductList  from './page/productlist/ProductList';
import Detail from './page/detail/Detail';
import About  from './page/about/About';
import Funding  from './page/funding/Funding';
import ContactUs  from './page/contactus/ContactUs';

import Header from './component/header/Header';
import Footer from './component/footer/Footer';
import Test from "./page/test/Test";


function App() {
	
	const [content, setContent] = useState(null);
	
	const DBGLOG = (msg) => {
		//console.log(msg);
	}
	
    useEffect(() => {
        Promise.all([
			fetch('/data/common.json'),
			fetch('/data/home.json'),
            fetch('/data/categories.json'),
            fetch('/data/products.json')
        ]).then(responses => {
            // Get a JSON object from each of the responses
            return Promise.all(responses.map(function (response) {
                return response.json();
            }));
        }).then(data => {
            // Log the data to the console
            // You would do something with both sets of data here
            DBGLOG("Result")
            DBGLOG(data);
			
			var content = {};
			content.common = data[0]; //common.json
			content.home = data[1]; //home.json
			content.categories = data[2]; //categories.json
			content.products = data[3]; //products.json
			
            setContent(content);
        }).catch(error => {
            // if there's an error, log it
            console.error("Home Page, Error fetching data : ", error) ;
            //setError(error);
        })
        .finally (() => {
            DBGLOG("finally block called");
            //setLoading(false)
        });
    }, []);  	
	
	
	if (!content) {
		return (
			<div><p>Loading....</p></div>
		)
	}

	return (
		<BrowserRouter>
		  <div>

			<div className="wrapper" align="center">
			  <div className="header"  align="left">
				<Header header={content.common.header}/>
			  </div>

			  <div className="content">
				<div className="sections">
					<Switch>
						<Route exact path="/"  render = {() => <Home homeContent={content.home} categories={content.categories} products={content.products}/> }/>
						<Route exact path="/scout"  render = {() => <ProductList products={content.products} categories={content.categories}/> }/>
						<Route exact path="/scout/:categoryId"  render = {() => <ProductList products={content.products} categories={content.categories}/> }/>
						<Route exact path="/detail/:productId"  render = {() => <Detail products={content.products} categories={content.categories}/> }/>
						<Route exact path="/about"  render = {() => <About/> }/>
						<Route exact path="/funding"  render = {() => <Funding/> }/>
						<Route exact path="/contactus"  render = {() => <ContactUs contactInfo={content.common.footer.contactInfo}/> }/>
						<Route exact path="/test"  render = {() => <Test/> }/>
					</Switch>
				</div>
			  </div>

			  <div className="footer" align="left">
				<Footer footer={content.common.footer}></Footer>
			  </div>
			  
			</div>
		  </div>
		</BrowserRouter> 
	);
}

export default App;
