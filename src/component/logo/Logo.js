function Logo (props) {
    const imageURL = props?.imageURL;
    const link = props?.link;
    const alt = props?.alt;
    const className = props?.className;

    return (
        <>
            <a class="navbar-brand" href={link}><img src={imageURL} class={className} alt={alt}/></a>
        </>
    );
}
export default Logo;

// Usage
// <Logo imageURL="" link="" alt="" className="">