import { Link } from "react-router-dom";
import { useState } from "react";

import './Header.css';

function Header(props) {
	
	const [data, setData] = useState({
		"toggle" : 0,
		"cssCollapse" : "rndo-collapsed",
		"cssExpand" : ""				
	});
	
	const toggle = (e) => {
		if(data.toggle) {
			setData({
				"toggle" : 0,
				"cssCollapse" : "rndo-collapsed",
				"cssExpand" : ""			
			});
		} else {
			setData({
				"toggle" : 1,
				"cssCollapse" : "",
				"cssExpand" : "in"
			});
		}
	}
	
	const ToggleIcon = () => {
	
		if (data.toggle) {
			return [
				<div className="toggle-icon">
					<div className="toggle-close-icon">X</div>
				</div>
			]
		
		} else {
			return [
					<div className="toggle-icon">
						<div className="toggle-expand-icon"></div>
						<div className="toggle-expand-icon"></div>
						<div className="toggle-expand-icon"></div>
					</div>					
			]			
		}
	}
	
    return(
        <>
        <div className="rndo-header">
            <div className="container">
                <button className={"navbar-toggle"} data-target="#navbar-menu" data-toggle="collapse" type="button" onClick={toggle}>
					<ToggleIcon/>
				</button>

                <div className="navbar-header rndo-navbar-header">
                    <Link to={props.header.logo.link}><img alt="" className="logo logo-scrolled" src={props.header.logo.imgURL} /></Link>
                </div>

                <div className={"collapse navbar-collapse " + data.cssExpand} id="navbar-menu">
					<ul className="nav navbar-nav rndo-navbar-left" data-in="fadeInDown" data-out="fadeOutUp">
						<li className="active"><input className="form-control rndo-search" placeholder="Find Freelancer" type="text" /></li>
						{
							props.header.menus.map((menu, index) =>
									<li key={index} className="dropdown"><Link to={menu.link} className="rndo-theme-menu-list" onClick={toggle}>{menu.label}</Link></li>
								)	
						}					
					</ul>

					<ul className="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
						{/* <li><a href="/">SignUp</a></li>
						<li className="left-br"><a className="signin" data-target="#signup" data-toggle="modal" href="/)">Sign In Now</a></li> */}
						<li className="dropdown"><Link to={props.header.contactUs.link} className="rndo-theme-menu-list" onClick={toggle}>{props.header.contactUs.label}</Link></li>
					</ul>
                </div>
            </div>
        </div>
        </>
    );
}

export default Header;