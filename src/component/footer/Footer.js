import './Footer.css';
import logo from '../../assets/img/logo.png';
import { Link } from 'react-router-dom';

function Footer(props) {
    const contactInfo = props.footer.contactInfo;
    const footerList = props.footer.list;
    const copyRightText = props.footer.copyRightText;
    // const socialLinks = props.footer.socialLinks;

    return(
        <>
            <footer className="dark-footer skin-dark-footer">
                <div className="dark-footer skin-dark-footer"> 
                        <div className="container">
                            <div className="row">
                                <div key="contactInfo" className="col-lg-3 col-md-3">
                                    <div key="contactInfo2" className="footer-widget">
                                        <img src={logo} className="img-footer" alt="" />
                                        <div className="footer-add">

                                            <p>{contactInfo.addr}</p>
                                            <label>Email:  {contactInfo.email}</label>
                                        </div>
                                        
                                    </div>
                                </div>		

                                {
                                    footerList.map ((footerListItem, id) => 
                                        <div key={'footer-div-1-' + id} className="col-lg-2 col-md-2">
                                            <div  key={'footer-div-2-'+ id} className="footer-widget">
                                                <h4 key={'footer-h4-1-'+ id} className="widget-title">{footerListItem.name}</h4>
                                                <ul key={id} className="footer-menu">

                                                    {
                                                        footerListItem.items.map((item, id2) =>                                                                
                                                                <li><Link key={id + id2} to={item.link}>{item.name}</Link></li>
                                                            )
                                                    }
                                                </ul>
                                            </div>
                                        </div>                                        
                                        )
                                }
                                
                                {/* <div className="col-lg-3 col-md-3 rndo-display-none">
                                    <div className="footer-widget">
                                        <h4 className="widget-title">Download Apps</h4>
                                        <a href="#" className="other-store-link">
                                            <div className="other-store-app">
                                                <div className="os-app-icon">
                                                    <i className="ti-android theme-cl"></i>
                                                </div>
                                                <div className="os-app-caps">
                                                    Google Play
                                                    <span>Get It Now</span>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="#" className="other-store-link">
                                            <div className="other-store-app">
                                                <div className="os-app-icon">
                                                    <i className="ti-apple theme-cl"></i>
                                                </div>
                                                <div className="os-app-caps">
                                                    App Store
                                                    <span>Now it Available</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div> */}
                                
                            </div>
                        </div>
                    </div>
                    
                    <div className="footer-bottom">
                        <div className="container">
                            <div className="row align-items-center">
                                
                                <div className="col-lg-6 col-md-6">
                                    <p className="mb-0">{copyRightText}</p>
                                </div>
                                
                                {/* <div className="col-lg-6 col-md-6 text-right rndo-display-none ">
                                    <ul className="footer-bottom-social">
                                        <li><a href="#"><i className="ti-facebook"></i></a></li>
                                        <li><a href="#"><i className="ti-twitter"></i></a></li>
                                        <li><a href="#"><i className="ti-instagram"></i></a></li>
                                        <li><a href="#"><i className="ti-linkedin"></i></a></li>
                                    </ul>
                                </div> */}
                                
                            </div>
                        </div>
                    </div>
            </footer>
        </>
    );
}

export default Footer;